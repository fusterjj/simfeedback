# SimFeedback

This project and the right hardware allows you to add haptic feedback to your DCS sim experience.
It uses tiny vibe motors that you can strap to a leg or arm with an Ace bandage.  The motors are driven by
a USB-connected Arduino.

# Parts
* Qty 4 [2N7000 MOSFET](https://www.mouser.com/ProductDetail/ON-Semiconductor/2N7000?qs=sGAEpiMZZMshyDBzk1%2FWi9bHELEahoDnY1fyKF6A6Ko%3D)
* Qty 4 [4.7kohm resistors](https://www.mouser.com/ProductDetail/Xicon/299-47K-RC?qs=sGAEpiMZZMu61qfTUdNhG%2Fbdyz6pU6a%252BfqpVSzaacno%3D)
* Qty 4 [Diodes](https://www.mouser.com/ProductDetail/ON-Semiconductor-Fairchild/1N4148TA?qs=sGAEpiMZZMtoHjESLttvkhO8aP%252BwY5nLJ%252BwPLAzkHD4%3D)
* Qty 4 [Vibration motors](https://www.sparkfun.com/products/8449)
* Qty 1 [2xAAA battery pack](https://www.sparkfun.com/products/14219)
* Qty 1 [Arduino Micro](https://www.sparkfun.com/products/12640)

# Schematic
![Schematic](schematic.png)

# Software Components
* [Arduino](arduino) - This is the code for the Arduino.  It accepts commands from the client over the virtual serial port via USB.
* [Client](client) - This is a Python 3 program that connects to DCS to get the acceleration data
* [DCS Export Core](dcs-export-core) - This is a fork of the dcs-export-core from the DCS BIOS team.  It was modified to export the
  necessary values in a vehicle-neutral way.

# Installation
You need [Python 3](https://www.python.org/downloads/windows/) and the [Arduino IDE](https://www.arduino.cc/en/Main/Software).

1. Copy the contents of dcs-export-core to your `c:\users\<username>\Saved Games\DCS` directory (or DCS.openbeta)
2. Open up the Arduino project and program the Micro over USB
3. Run the simfeedback.py client
4. Start DCS
5. ...
6. Profit

# Future improvements
It might be nice to skip the simfeedback.py client and implement everything directly in Lua.  The hardware could
be an ESP8266 or similar which would directly connect to DCS.

# References
* [DCS Export Core](https://github.com/jboecker/dcs-export-core)
* DCS Lua API: `<DCS game folder>\API`


