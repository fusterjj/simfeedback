#define AXIS1_M1_PIN 5
#define AXIS1_M2_PIN 6
#define AXIS2_M1_PIN 9
#define AXIS2_M2_PIN 10

#include "iomsg.h"

void setup() {
  pinMode(AXIS1_M1_PIN, OUTPUT);
  pinMode(AXIS1_M2_PIN, OUTPUT);
  pinMode(AXIS2_M1_PIN, OUTPUT);
  pinMode(AXIS2_M2_PIN, OUTPUT);

  Serial.begin(115200);
  // Wait for USB serial to be connected
  while (!Serial);

}

void loop() {
    uint8_t b;
    uint32_t command;
    uint8_t commandData[8];
    PARSE_STATE_t parseState;
  
    if (Serial.available() > 0) {
      b = Serial.read();
      parseState = parse_iomsg(b, &command, commandData);

      if (PARSE_STATE_COMPLETE == parseState) {
          analogWrite(AXIS1_M1_PIN, commandData[0]);
          analogWrite(AXIS1_M2_PIN, commandData[1]);
          analogWrite(AXIS2_M1_PIN, commandData[2]);
          analogWrite(AXIS2_M2_PIN, commandData[3]);

      }
    }

}
