#include <Arduino.h>
#include <string.h>

#include "iomsg.h"

//#define DEBUG(x) espSerial.println(x)
#define DEBUG(x)

//extern "C" {

// Parses IOMSG messages from a serial stream
PARSE_STATE_t parse_iomsg(uint8_t b, uint32_t *command, uint8_t *data) {
    static PARSE_STATE_t parseState = PARSE_STATE_SYNC1;
    static int payloadCount = 0;
    static uint8_t payload[IOMSG_PAYLOAD_LEN];

    switch (parseState) {
        default:
            parseState = PARSE_STATE_SYNC1;
        case PARSE_STATE_SYNC1:
            DEBUG("ST SYNC1");
            if (IOMSG_SYNC1 == b) {
                parseState = PARSE_STATE_SYNC2;
                DEBUG("RX SYNC1");
            }
            break;

        case PARSE_STATE_SYNC2:
            DEBUG("ST SYNC2");
            if (IOMSG_SYNC2 == b) {
                parseState = PARSE_STATE_SYNC3;
                DEBUG("RX SYNC2");
            } else {
                parseState = PARSE_STATE_SYNC1;
            }
            break;

        case PARSE_STATE_SYNC3:
            DEBUG("ST SYNC3");
            if (IOMSG_SYNC3 == b) {
                parseState = PARSE_STATE_SYNC4;
                DEBUG("RX SYNC3");
            } else {
                parseState = PARSE_STATE_SYNC1;
            }
            break;

        case PARSE_STATE_SYNC4:
            DEBUG("ST SYNC4");
            if (IOMSG_SYNC4 == b) {
                parseState = PARSE_STATE_FULL_MSG;
                DEBUG("RX SYNC4");
                payloadCount = 0;
            } else {
                parseState = PARSE_STATE_SYNC1;
            }
            break;

        case PARSE_STATE_FULL_MSG:
            DEBUG("ST FULL_MSG");
            payload[payloadCount] = b;
            payloadCount++;
            if (IOMSG_PAYLOAD_LEN == payloadCount) {
                DEBUG("RX FULL_MSG");
                memcpy((void*)command, (void*)&payload[0], 4);
                memcpy((void*)data, (void*)&payload[4], 8);
                parseState = PARSE_STATE_COMPLETE;
            }
            break;
    } // State switch

    return parseState;
}

//}
