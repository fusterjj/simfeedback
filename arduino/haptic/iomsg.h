#define IOMSG_LENGTH        (16)
#define IOMSG_PAYLOAD_LEN   (12)
#define IOMSG_SYNC1         (0xd1U)
#define IOMSG_SYNC2         (0x1dU)
#define IOMSG_SYNC3         (0xfeU)
#define IOMSG_SYNC4         (0xedU)

#define MAX_SERVOS          (4)
#define MAX_ANALOG_PIN      (7) // Mini Pro

#define MIN_SERVO_CMD       (500)
#define MAX_SERVO_CMD       (2000)

#define MAX_SAMPLES         (1000)

#define IOMSG_GPIO_OUTPUT   (0)
#define IOMSG_GPIO_INPUT_PULLUP (1)
#define IOMSG_GPIO_INPUT    (2)

//extern "C" {
    enum {
        IOMSG_SERVO_WRITE       = 0x00000001,
        IOMSG_AN_READ           = 0x00000002,
        IOMSG_AN_READ_RESP      = 0x80000002,
        IOMSG_AN_SAMPLE         = 0x00000003,
        IOMSG_AN_SAMPLE_RESP    = 0x80000003,
        IOMSG_GPIO_CONFIG       = 0x00000004,
        IOMSG_GPIO_WRITE        = 0x00000005,
        IOMSG_GPIO_READ         = 0x00000006,
        IOMSG_GPIO_READ_RESP    = 0x80000006,
        IOMSG_SERVO_CONFIG      = 0x00000007,
    };

    typedef enum {
        PARSE_STATE_SYNC1,
        PARSE_STATE_SYNC2,
        PARSE_STATE_SYNC3,
        PARSE_STATE_SYNC4,
        PARSE_STATE_FULL_MSG,
        PARSE_STATE_COMPLETE
    } PARSE_STATE_t;

    PARSE_STATE_t parse_iomsg(uint8_t b, uint32_t *command, uint8_t *data);
//}

