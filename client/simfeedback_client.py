
import socket
import json
import time
import struct
from serial import Serial
from serial.serialutil import SerialTimeoutException,SerialException
import serial.tools.list_ports as list_ports
import sys
from math import exp, log, fabs

# X, Y (up/down), Z (left/right)
ROTARY_WING_MAX_G = (3.0, 2.0, 1.0)
FIXED_WING_MAX_G = (9.0, 9.0, 1.0)
EXPO_X = 0.00
EXPO_Y = -0.25
EXPO_Z = -0.25
MAX_PWM = 255.0
ROTARY_WINGS = ('UH-1H', 'Mi-8MT', 'Ka-50', 'SA342L', 'SA342M', 'SA342Minigun', 'SA342Mistral', 'Mi-24P')

TEST_MODE = False

sf = None
s = None
serport = None

def expo(x, k):
    if fabs(x) > 0.001:
        y = exp(log(x)*pow(10,k))
    else:
        y = x

    return y

def dcsConnect():
    global sf
    global s

    connected = False
    while not connected:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect(("127.0.0.1", 12800))
            s.settimeout(2.0)
            sf = s.makefile()
            sendMessage(
                    {"action":"subscribe", "keys":["state"]}
            )
            connected = True
        except ConnectionRefusedError:
            print('Trying DCS connection again...')

def sendMessage(msg):
    # json.dumps will not do any pretty-printing, so we do not have to remove
    # newlines from its output
    s.send( (json.dumps(msg) + "\n").encode("ascii") )

def setPWM(motor1, motor2, motor3, motor4):
    global serport

    serialCmd = struct.pack('<BBBBLBBBBBBBB', 0xd1, 0x1d, 0xfe, 0xed, 0x00000000, 
            motor1, motor2, motor3, motor4, 0, 0, 0, 0)
    serport.write(serialCmd)

def serialConnect():
    global serport

    # Pick a serial port
    fbport = None
    try:
        while fbport is None:
            for portname,description,_ in list_ports.comports():
                #print("{0}: {1}".format(portname, description))
                if "Arduino" in description:
                    fbport = portname
                    fbportdesc = description

            if fbport is None:
                print("Could not find an Arduino serial port, trying again")
                time.sleep(1.0)

    except KeyboardInterrupt:
        print("Exiting...")
        sys.exit()


    print("Using {0}: {1}".format(fbport, fbportdesc))

    serport = Serial(fbport)
    setPWM(0, 0, 0, 0)

timestamp_ms = lambda: int(round(time.time() * 1000))

############ main ###############


##### Main loop
dcsConnected = False
serialConnected = False
frameCount = 0
testDataCount = 0
testDataInc = 1
lastTime = timestamp_ms()
unitType = None
max_g = FIXED_WING_MAX_G
while True:
    try:
        if not serialConnected:
            serialConnect()
            serialConnected = True

        if not TEST_MODE:
            if not dcsConnected:
                dcsConnect()
                dcsConnected = True

            try:
                msg = json.loads(sf.readline())
            except (json.JSONDecodeError, OSError):
                print("Failed reading data from DCS")
                setPWM(0,0,0,0)
                dcsConnected = False
                time.sleep(1.0)
                continue
            except (socket.timeout):
                print("Timeout waiting for data from DCS")
                setPWM(0,0,0,0)
                time.sleep(1.0)
                continue

        else:
            # Generate some fake data
            time.sleep(0.033)
            testDataCount = testDataCount + testDataInc
            if abs(testDataCount) == 60:
                testDataInc = -testDataInc

            msg = {}
            msg['msg_type'] = "newdata"
            msg['data'] = {}
            msg['data']['AccelX'] = max_g[0] * testDataCount / 60.0
            msg['data']['AccelY'] = max_g[1] * testDataCount / 60.0
            msg['data']['AccelZ'] = max_g[2] * testDataCount / 60.0


        if "msg_type" in msg:
            if msg["msg_type"] == "new_unit":
                print("New unit: {}".format(msg["type"]))

            if msg["msg_type"] == "newdata":
                # Check for unit type change
                if msg['data']['_UNITTYPE'] != unitType:
                    unitType = msg['data']['_UNITTYPE']
                    print("Unit type changed: {}".format(unitType))
                    if unitType in ROTARY_WINGS:
                        print("Using rotary wing configuration")
                        max_g = ROTARY_WING_MAX_G
                    else:
                        print("Using fixed wing configuration")
                        max_g = FIXED_WING_MAX_G

                frameCount = frameCount + 1
                timestamp = timestamp_ms()
                frameTime = timestamp - lastTime
                lastTime = timestamp
                #print("frameTime: {}, frameCount: {}".format(frameTime, frameCount))
                #if "IndicatedAirSpeed" in msg["data"]:
                #    print("IAS: {}".format(msg["data"]["IndicatedAirSpeed"]))
                #if "AccelX" in msg["data"]:
                #    print("Accel: {}, {}, {}".format(msg["data"]["AccelX"], msg["data"]["AccelY"], msg["data"]["AccelZ"]))
                #if "Pitch" in msg["data"]:
                #    print("Pitch/Bank/Yaw: {}, {}, {}".format(msg["data"]["Pitch"], msg["data"]["Bank"], msg["data"]["Yaw"]))
                #accelX = min(MAX_G_X, msg['data']['AccelX'])
                accelXPos = max(0, min(max_g[0], msg['data']['AccelX']))
                accelXNeg = -max(-max_g[0], min(0, msg['data']['AccelX']))
                accelYPos = max(0, min(max_g[1], (msg['data']['AccelY'] - 1.0)))
                accelYNeg = -max(-max_g[1], min(0, (msg['data']['AccelY'] - 1.0)))
                accelZPos = max(0, min(max_g[2], msg['data']['AccelZ']))
                accelZNeg = -max(-max_g[2], min(0, msg['data']['AccelZ']))

                if frameCount % 15 == 0:
                    print("+X:{:+.2f} -X:{:+.2f} +Y:{:+.2f} -Y:{:+.2f} +Z:{:+.2f} -Z:{:+.2f}".format(
                        accelXPos, accelXNeg, accelYPos, accelYNeg, accelZPos, accelZNeg))

                accelYPosScaled  = expo(accelYPos / max_g[1], EXPO_Y)
                accelYNegScaled  = expo(accelYNeg / max_g[1], EXPO_Y)
                accelZPosScaled  = expo(accelZPos / max_g[2], EXPO_Z)
                accelZNegScaled  = expo(accelZNeg / max_g[2], EXPO_Z)

                accelYPosPWM = int(MAX_PWM * accelYPosScaled)
                accelYNegPWM = int(MAX_PWM * accelYNegScaled)
                accelZPosPWM = int(MAX_PWM * accelZPosScaled)
                accelZNegPWM = int(MAX_PWM * accelZNegScaled)

                setPWM(accelYNegPWM, accelYPosPWM, accelZPosPWM, accelZNegPWM)
    except ConnectionResetError:
        print("Lost DCS connection, trying again")
        dcsConnected = False
        continue
    except (SerialTimeoutException, SerialException):
        print("Lost serial connection, trying again")
        serialConnected = False
        continue

    except KeyboardInterrupt:
        print("Exiting...")
        setPWM(0,0,0,0)
        sys.exit()

