package.path  = package.path..";.\\LuaSocket\\?.lua"
package.cpath = package.cpath..";.\\LuaSocket\\?.dll"
  
socket = require("socket")
lfs = require("lfs")

SimFBExportCore = {}
function SimFBExportCore.init()
	dofile(lfs.writedir()..[[Scripts\SimFBExportCore\Network.lua]])
	dofile(lfs.writedir()..[[Scripts\SimFBExportCore\KeyValueStore.lua]])
	dofile(lfs.writedir()..[[Scripts\SimFBExportCore\Protocol.lua]])
	SimFBExportCore.KVS.init({sendMessage = SimFBExportCore.NET.sendMessage})
	SimFBExportCore.NET.init({processMessage = SimFBExportCore.PROTOCOL.processMessage})
	SimFBExportCore.PROTOCOL.init({sendMessage = SimFBExportCore.NET.sendMessage, KVS = SimFBExportCore.KVS})
end

-- Prev Export functions.
local PrevExport = {}
PrevExport.LuaExportStart = LuaExportStart
PrevExport.LuaExportStop = LuaExportStop
PrevExport.LuaExportBeforeNextFrame = LuaExportBeforeNextFrame
PrevExport.LuaExportAfterNextFrame = LuaExportAfterNextFrame

-- Lua Export Functions
function LuaExportStart()
	
	
	-- Chain previously-included export as necessary
	if PrevExport.LuaExportStart then
		PrevExport.LuaExportStart()
	end
end

LuaExportStop = function()
	
	SimFBExportCore.NET.stop()
	
	-- Chain previously-included export as necessary
	if PrevExport.LuaExportStop then
		PrevExport.LuaExportStop()
	end
end

function LuaExportBeforeNextFrame()
	
	SimFBExportCore.NET.step()
	SimFBExportCore.PROTOCOL.step()
	
	-- Chain previously-included export as necessary
	if PrevExport.LuaExportBeforeNextFrame then
		PrevExport.LuaExportBeforeNextFrame()
	end
	
end

function LuaExportAfterNextFrame()
	
	SimFBExportCore.NET.step()

	-- Chain previously-included export as necessary
	if PrevExport.LuaExportAfterNextFrame then
		PrevExport.LuaExportAfterNextFrame()
	end
end

SimFBExportCore.init()
