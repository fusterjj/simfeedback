EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:2N7000 Q4
U 1 1 5EB4D00A
P 3200 1700
F 0 "Q4" H 3405 1746 50  0000 L CNN
F 1 "2N7000" H 3405 1655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3400 1625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 3200 1700 50  0001 L CNN
	1    3200 1700
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 5EB61059
P 3400 1900
F 0 "R4" H 3468 1946 50  0000 L CNN
F 1 "4.7k" H 3468 1855 50  0000 L CNN
F 2 "" H 3400 1900 50  0001 C CNN
F 3 "~" H 3400 1900 50  0001 C CNN
	1    3400 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 5EB607B0
P 2850 2450
F 0 "R3" H 2918 2496 50  0000 L CNN
F 1 "4.7k" H 2918 2405 50  0000 L CNN
F 2 "" H 2850 2450 50  0001 C CNN
F 3 "~" H 2850 2450 50  0001 C CNN
	1    2850 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2600 4300 2600
Wire Wire Line
	3400 1700 3400 1800
$Comp
L simfeedback-rescue:ProMicro-promicro U1
U 1 1 5EB4E334
P 5000 2850
F 0 "U1" H 5000 3887 60  0000 C CNN
F 1 "ProMicro" H 5000 3781 60  0000 C CNN
F 2 "" H 5100 1800 60  0000 C CNN
F 3 "" H 5100 1800 60  0000 C CNN
	1    5000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2400 4300 2300
$Comp
L power:GND #PWR?
U 1 1 5EB6696C
P 3950 2300
F 0 "#PWR?" H 3950 2050 50  0001 C CNN
F 1 "GND" H 3955 2127 50  0000 C CNN
F 2 "" H 3950 2300 50  0001 C CNN
F 3 "" H 3950 2300 50  0001 C CNN
	1    3950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2300 4300 2300
Connection ~ 4300 2300
$Comp
L power:GND #PWR?
U 1 1 5EB67E35
P 6050 2250
F 0 "#PWR?" H 6050 2000 50  0001 C CNN
F 1 "GND" H 6055 2077 50  0000 C CNN
F 2 "" H 6050 2250 50  0001 C CNN
F 3 "" H 6050 2250 50  0001 C CNN
	1    6050 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2200 6050 2250
Wire Wire Line
	3400 2000 3100 2000
Wire Wire Line
	3100 2000 3100 1900
Wire Wire Line
	4300 2500 3750 2500
Wire Wire Line
	3400 1700 3750 1700
Wire Wire Line
	3750 1700 3750 2500
Connection ~ 3400 1700
Wire Wire Line
	2550 2550 2850 2550
Connection ~ 3100 2000
Wire Wire Line
	3500 2600 3500 2350
$Comp
L power:GND #PWR?
U 1 1 5EB8B7D6
P 2550 2550
F 0 "#PWR?" H 2550 2300 50  0001 C CNN
F 1 "GND" H 2555 2377 50  0000 C CNN
F 2 "" H 2550 2550 50  0001 C CNN
F 3 "" H 2550 2550 50  0001 C CNN
	1    2550 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB8BF1B
P 3100 2000
F 0 "#PWR?" H 3100 1750 50  0001 C CNN
F 1 "GND" H 3105 1827 50  0000 C CNN
F 2 "" H 3100 2000 50  0001 C CNN
F 3 "" H 3100 2000 50  0001 C CNN
	1    3100 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR?
U 1 1 5EB952D0
P 2550 1750
F 0 "#PWR?" H 2550 1600 50  0001 C CNN
F 1 "+3V0" H 2565 1923 50  0000 C CNN
F 2 "" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7000 Q2
U 1 1 5EBB0C02
P 2900 3500
F 0 "Q2" H 3105 3546 50  0000 L CNN
F 1 "2N7000" H 3105 3455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3100 3425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 2900 3500 50  0001 L CNN
	1    2900 3500
	-1   0    0    -1  
$EndComp
$Comp
L pspice:DIODE D2
U 1 1 5EBB0C08
P 2800 3100
F 0 "D2" V 2800 2972 50  0000 R CNN
F 1 "DIODE" H 2800 3274 50  0001 C CNN
F 2 "" H 2800 3100 50  0001 C CNN
F 3 "~" H 2800 3100 50  0001 C CNN
	1    2800 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 5EBB0C0E
P 3100 3700
F 0 "R2" H 3168 3746 50  0000 L CNN
F 1 "4.7k" H 3168 3655 50  0000 L CNN
F 2 "" H 3100 3700 50  0001 C CNN
F 3 "~" H 3100 3700 50  0001 C CNN
	1    3100 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 5EBB0C14
P 3900 4600
F 0 "R1" H 3968 4646 50  0000 L CNN
F 1 "4.7k" H 3968 4555 50  0000 L CNN
F 2 "" H 3900 4600 50  0001 C CNN
F 3 "~" H 3900 4600 50  0001 C CNN
	1    3900 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3500 3100 3600
$Comp
L Transistor_FET:2N7000 Q1
U 1 1 5EBB0C1C
P 3700 4500
F 0 "Q1" H 3905 4546 50  0000 L CNN
F 1 "2N7000" H 3905 4455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3900 4425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 3700 4500 50  0001 L CNN
	1    3700 4500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 3800 2800 3800
Wire Wire Line
	2800 3800 2800 3700
Wire Wire Line
	3100 3500 3250 3500
Connection ~ 3100 3500
Wire Wire Line
	3600 4700 3900 4700
Connection ~ 2800 3800
$Comp
L pspice:DIODE D1
U 1 1 5EBB0C2E
P 3600 4100
F 0 "D1" V 3600 3972 50  0000 R CNN
F 1 "DIODE" H 3600 4274 50  0001 C CNN
F 2 "" H 3600 4100 50  0001 C CNN
F 3 "~" H 3600 4100 50  0001 C CNN
	1    3600 4100
	0    -1   -1   0   
$EndComp
Connection ~ 3600 4700
$Comp
L power:GND #PWR?
U 1 1 5EBB0C36
P 3600 4700
F 0 "#PWR?" H 3600 4450 50  0001 C CNN
F 1 "GND" H 3605 4527 50  0000 C CNN
F 2 "" H 3600 4700 50  0001 C CNN
F 3 "" H 3600 4700 50  0001 C CNN
	1    3600 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EBB0C3C
P 2800 3800
F 0 "#PWR?" H 2800 3550 50  0001 C CNN
F 1 "GND" H 2805 3627 50  0000 C CNN
F 2 "" H 2800 3800 50  0001 C CNN
F 3 "" H 2800 3800 50  0001 C CNN
	1    2800 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR?
U 1 1 5EBB0C42
P 2800 2900
F 0 "#PWR?" H 2800 2750 50  0001 C CNN
F 1 "+3V0" H 2815 3073 50  0000 C CNN
F 2 "" H 2800 2900 50  0001 C CNN
F 3 "" H 2800 2900 50  0001 C CNN
	1    2800 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR?
U 1 1 5EBB0C48
P 3600 3850
F 0 "#PWR?" H 3600 3700 50  0001 C CNN
F 1 "+3V0" H 3615 4023 50  0000 C CNN
F 2 "" H 3600 3850 50  0001 C CNN
F 3 "" H 3600 3850 50  0001 C CNN
	1    3600 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 5EBB0C58
P 1650 4200
F 0 "J?" H 1542 4293 50  0001 C CNN
F 1 "Conn_01x02_Female" H 1678 4085 50  0001 L CNN
F 2 "" H 1650 4200 50  0001 C CNN
F 3 "~" H 1650 4200 50  0001 C CNN
	1    1650 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 3850 3600 3900
Wire Wire Line
	5700 2200 6050 2200
Wire Wire Line
	1850 1400 2550 1400
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 5EB9B1AD
P 1650 1400
F 0 "J?" H 1542 1493 50  0001 C CNN
F 1 "Conn_01x02_Female" H 1678 1285 50  0001 L CNN
F 2 "" H 1650 1400 50  0001 C CNN
F 3 "~" H 1650 1400 50  0001 C CNN
	1    1650 1400
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR?
U 1 1 5EB8D03B
P 3100 1100
F 0 "#PWR?" H 3100 950 50  0001 C CNN
F 1 "+3V0" H 3115 1273 50  0000 C CNN
F 2 "" H 3100 1100 50  0001 C CNN
F 3 "" H 3100 1100 50  0001 C CNN
	1    3100 1100
	1    0    0    -1  
$EndComp
$Comp
L pspice:DIODE D4
U 1 1 5EB4FD0D
P 3100 1300
F 0 "D4" V 3100 1172 50  0000 R CNN
F 1 "DIODE" H 3100 1474 50  0001 C CNN
F 2 "" H 3100 1300 50  0001 C CNN
F 3 "~" H 3100 1300 50  0001 C CNN
	1    3100 1300
	0    -1   -1   0   
$EndComp
Connection ~ 3100 1500
Wire Wire Line
	1850 1500 3100 1500
Wire Wire Line
	2550 1100 3100 1100
Wire Wire Line
	2550 1100 2550 1400
Connection ~ 3100 1100
Wire Wire Line
	2850 2350 3500 2350
Connection ~ 3600 4300
Wire Wire Line
	4200 4500 3900 4500
Connection ~ 3900 4500
Wire Wire Line
	4300 3000 4200 3000
Wire Wire Line
	2250 2900 2800 2900
Connection ~ 2800 2900
$Comp
L pspice:DIODE D3
U 1 1 5EB89CC9
P 2550 1950
F 0 "D3" V 2550 1822 50  0000 R CNN
F 1 "DIODE" H 2550 2124 50  0001 C CNN
F 2 "" H 2550 1950 50  0001 C CNN
F 3 "~" H 2550 1950 50  0001 C CNN
	1    2550 1950
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_FET:2N7000 Q3
U 1 1 5EB6EF11
P 2650 2350
F 0 "Q3" H 2855 2396 50  0000 L CNN
F 1 "2N7000" H 2855 2305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2850 2275 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 2650 2350 50  0001 L CNN
	1    2650 2350
	-1   0    0    -1  
$EndComp
Connection ~ 2550 2550
Connection ~ 2850 2350
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 5EC06C8B
P 1650 2050
F 0 "J?" H 1542 2143 50  0001 C CNN
F 1 "Conn_01x02_Female" H 1678 1935 50  0001 L CNN
F 2 "" H 1650 2050 50  0001 C CNN
F 3 "~" H 1650 2050 50  0001 C CNN
	1    1650 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1850 2150 2550 2150
Connection ~ 2550 2150
Wire Wire Line
	2550 1750 1850 1750
Wire Wire Line
	1850 1750 1850 2050
Connection ~ 2550 1750
Wire Wire Line
	2250 2900 2250 3200
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 5EBB0C4E
P 1650 3200
F 0 "J?" H 1542 3293 50  0001 C CNN
F 1 "Conn_01x02_Female" H 1678 3085 50  0001 L CNN
F 2 "" H 1650 3200 50  0001 C CNN
F 3 "~" H 1650 3200 50  0001 C CNN
	1    1650 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1850 3200 2250 3200
Wire Wire Line
	1850 3300 2800 3300
Connection ~ 2800 3300
Wire Wire Line
	1850 4300 3600 4300
Wire Wire Line
	1850 4200 3050 4200
Wire Wire Line
	3600 3900 3050 3900
Wire Wire Line
	3050 3900 3050 4200
Connection ~ 3600 3900
Text Notes 1550 2350 1    50   ~ 0
Axis1 Motor 2
Text Notes 1550 1650 1    50   ~ 0
Axis1 Motor1
Text Notes 1550 3450 1    50   ~ 0
Axis2 Motor1
Text Notes 1550 4500 1    50   ~ 0
Axis2 Motor2
Wire Wire Line
	4200 3000 4200 4500
Wire Wire Line
	3250 2900 4300 2900
Wire Wire Line
	3250 2900 3250 3500
$EndSCHEMATC
